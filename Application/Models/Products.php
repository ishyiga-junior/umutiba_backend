<?php

use Cloudinary\Api\Upload\UploadApi;
use Cloudinary\Configuration\Configuration;
use MVC\Model;

Configuration::instance([
    'cloud' => [
        'cloud_name' => 'julesntare',
        'api_key' => '658528737163627',
        'api_secret' => '_rLWat1SCgg0NMVSZ6myvZO99-Y',
    ],
    'url' => [
        'secure' => true,
    ],
]);

class ModelsProducts extends Model
{

    public function getAllProducts()
    {
        $products = $this->db->query("SELECT account_type, pi.added_on, email, full_name,
harvest_status, harvest_time, `image`, `is-device-id`, keyword, `location`, mobile_no,
`name`, niki_code, phone, price, prod_id, quality, quantity, pi.user_id FROM " . DB_PREFIX . "product_info pi left join " . DB_PREFIX . "users as u on pi.user_id = u.user_id ORDER BY added_on desc");
        for ($i = 0; $i < $products->num_rows; $i++) {
            $products->rows[$i]['location'] = json_decode($products->rows[$i]['location'], true);
        }

        return $products;
    }

    public function getAllNikiProducts()
    {
        $URL = "http://ishyiga.com:8080/NIKI/NikiCodeRequestUmutibaData.jsp";
        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: SDJUNSHDSD',
                'content' => '<timeToPull>2022-01-01 00:00:00</timeToPull>
<business_categorie>MARKET</business_categorie>',
            ),
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($URL, false, $context);

        return trim($result);
    }

    public function getSpecificProduct($value)
    {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_info where mobile_no = '$value' order by added_on desc");
        return $result;
    }

    public function getSpecificUserProducts($user_id)
    {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_info where user_id = '$user_id' order by added_on desc");
        return $result;
    }

    public function getAllProductsBySpecificCity($city)
    {
        $result = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'product_info where location->"$.cityName" = "' . $city . '" order by added_on desc');
        return $result;
    }

    public function getUserPoints($id)
    {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_bonus where user_id = '$id'");
        return $result;
    }

    public function saveProduct($data, $files)
    {
        $name = $data['name'] ?? null;
        $niki_code = $data['niki_code'] ?? null;
        $price = $data['price'] ?? null;
        $quantity = $data['quantity'] ?? null;
        $location = $data['location'] ?? null;
        $keywords = $data['keywords'] ?? null;
        $quality = $data['quality'] ?? null;
        $phone = $data['phone'] ?? null;
        $deviceId = $data['device_id'] ?? null;
        $harvestTime = $data['harvest_time'] ?? null;
        $userId = empty($deviceId) ? $data['user_id'] : $deviceId;
        $isDeviceId = empty($data['user_id']) ? 1 : 0;
        $date = date_create($harvestTime);
        $harvest_date = date_format($date, "Y-m-d");
        $currentDate = date('Y-m-d');

        $currentDate = date('Y-m-d', strtotime($currentDate));
        if ($currentDate == $harvest_date) {
            $harvest_status = 1;
        } else {
            $harvest_status = 0;
        }

        if (empty($niki_code) || empty($name) || empty($price) || empty($quantity) || empty($location) || empty($files['image']['tmp_name']) || empty($userId)) {
            return 404;
        }

        try {
            $uploaded = (new UploadApi())->upload(
                $files['image']['tmp_name'],
                [
                    'resource_type' => 'image',
                    'folder' => 'umutiba-images/',
                    'chunk_size' => $files['image']['size'],
                    'eager_async' => true,
                ]
            );
            $this->db->query("insert into " . DB_PREFIX . "product_info (user_id, niki_code, name, keyword, price, quantity, quality, location, image, phone, harvest_time, `is-device-id`, harvest_status) values('$userId', '$niki_code', '$name', '$keywords', '$price', '$quantity', '$quality', '$location', '" . $uploaded['url'] . "', '$phone', '$harvest_date', '$isDeviceId', '$harvest_status')");

            $user_id = $this->getUserPoints($userId);
            if ($user_id->num_rows > 0) {
                $points = $user_id->rows[0]['points'] + 1;
                $this->db->query("update " . DB_PREFIX . "user_bonus set points = '$points' where user_id = '$userId'");
            } else {
                $this->db->query("insert into " . DB_PREFIX . "user_bonus (user_id) values('$userId')");
            }
        } catch (PDOException $ex) {
            return 500;
        }

        return 201;
    }
}