<?php

use MVC\Model;

$dotenv = Dotenv\Dotenv::createImmutable(dirname(dirname(__DIR__)));
$dotenv->safeLoad();
class ModelsUsers extends Model
{

    public function getAllUsers()
    {
        $users = $this->db->query("SELECT user_id, full_name, mobile_no, email, account_type, ut.name as user_type_name FROM " . DB_PREFIX . "users as u inner join " . DB_PREFIX . "user_type ut on u.account_type = ut.id where account_type!=1");
        return $users;
    }

    public function getAllGuests()
    {
        $users = $this->db->query("SELECT user_id as device_id, location FROM " . DB_PREFIX . "product_info where `is-device-id`=1");
        for ($i = 0; $i < $users->num_rows; $i++) {
            $users->rows[$i]['location'] = json_decode($users->rows[$i]['location'], true);
        }

        return $users;
    }

    public function checkVerification($value)
    {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "users where mobile_no = '$value' AND is_verified=0");
        return $result;
    }

    public function getUser($value)
    {
        $result = $this->db->query("SELECT user_id, full_name, mobile_no, email FROM " . DB_PREFIX . "users where mobile_no = '$value'");
        return $result;
    }

    public function getSpecialUser($email, $password)
    {
        $pass = md5($password);
        $result = $this->db->query("SELECT user_id, full_name, mobile_no, email, account_type FROM " . DB_PREFIX . "users where email = '$email' && password ='$pass' && account_type = 1");
        return $result;
    }

    public function getUserById($id)
    {
        $result = $this->db->query("SELECT user_id, full_name, mobile_no, email, account_type FROM " . DB_PREFIX . "users where user_id = '$id'");
        return $result;
    }

    public function getUserByEmail($email)
    {
        $result = $this->db->query("SELECT user_id, full_name, mobile_no, email FROM " . DB_PREFIX . "users where email = '$email'");
        return $result;
    }

    public function saveUser($data)
    {
        $full_name = $data['full_name'];
        $email = $data['email'];
        $mobile_no = $data['phone'];
        $password = md5($data['password']);
        $account_type = $data['account_type'];
        $verify_code = $this->getVerificationCode();
        header("Content-Type: application/json; charset=UTF-8");
        $msg = "Welcome to Umutiba, your verification code: " . $verify_code;
        $data = array(
            "sender" => '+250780674459',
            "recipients" => $mobile_no,
            "message" => $msg,
        );

        $url = "https://www.intouchsms.co.rw/api/sendsms/.json";
        $data = http_build_query($data);
        $username = 'julesntare';
        $passcode = 'ju.jo.123.its';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $passcode);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        try {
            $saved = $this->db->query("insert into " . DB_PREFIX . "users (full_name, email, mobile_no, password, account_type, verification_code) values('$full_name', '$email', '$mobile_no', '$password', '$account_type', '$verify_code')");
        } catch (PDOException $ex) {
            return false;
        }

        return $saved;
    }

    public function authUser($data)
    {
        $mobile = $data['username'];
        $password = md5($data['password']);
        try {
            $saved = $this->db->query("select user_id, full_name, mobile_no, email, account_type, is_verified from " . DB_PREFIX . "users where mobile_no = '$mobile' and password = '$password' AND is_verified=1");
        } catch (PDOException $ex) {
            return false;
        }

        return $saved;
    }

    public function activateUser($data)
    {
        $code = $data['code'];
        $phone = $data['phone'];
        try {
            $result = $this->db->query("select * from " . DB_PREFIX . "users where is_verified = 0 AND mobile_no = '$phone' AND verification_code = '$code'");
            if ($result->num_rows > 0) {
                $this->db->query("update " . DB_PREFIX . "users set is_verified = 1, account_status = 1 where mobile_no = '$phone'");
                return 1;
            } else {
                return 2;
            }
        } catch (PDOException $ex) {
            return false;
        }
    }

    public function getVerificationCode()
    {
        return (int) substr(number_format(time() * rand(), 0, '', ''), 0, 5);
    }
}