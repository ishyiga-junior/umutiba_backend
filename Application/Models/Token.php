<?php

use MVC\Model;

class ModelsToken extends Model
{

    public function createToken($payload)
    {
        $Sql = "INSERT INTO " . DB_PREFIX . "auth_tokens (user_id, user_token) VALUES ('" . $payload['user_id'] . "', '" . $payload['jwt_token'] . "')";
        $result = $this->db->query($Sql);

        if ($result) {
            return array(
                'status' => true,
                'data' => $payload
            );
        }

        return array(
            'status' => false,
            'data' => []
        );
    }

    public function fetchToken($token)
    {
        $Sql = "SELECT * FROM " . DB_PREFIX . "auth_tokens WHERE user_token = '$token'";
        $result = $this->db->query($Sql);

        $Data = $result->fetch();
        if (!empty($Data)) {
            return array(
                'status' => true,
                'data' => $Data
            );
        }

        return array(
            'status' => false,
            'data' => []
        );
    }
}