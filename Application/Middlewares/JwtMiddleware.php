<?php

use MVC\Middleware;

use \Firebase\JWT\JWT;

class JwtMiddleware extends Middleware
{
    protected static $user;
    protected static $token;
    protected static $user_id;

    public static function JWTSecret()
    {
        return '-----BEGIN EC PRIVATE KEY-----
MHQCAQEEIDzQVg9bJ1kZFsZDoLeqadA4OTgKc40ukSmQ3MVzcV0soAcGBSuBBAAK
oUQDQgAEvzUNKCE3UVimCLUePomOUH/kfy0ujHdN5Kmn7ez3TtokJDy5ksVnOgf6
WzpmzY46zvKAnQ44Cgx5Kdqx5dVDiw==
-----END EC PRIVATE KEY-----';
    }

    protected static function getToken()
    {
        Self::$token = $_SERVER['HTTP_AUTHORIZATION'];
        return $_SERVER['HTTP_AUTHORIZATION'];
    }

    protected static function validateToken()
    {
        Self::getToken();
        if (Self::$token == '' || Self::$token == null) {
            return false;
        }

        try {
            $Token = explode('Bearer ', Self::$token);

            if (isset($Token[1]) && $Token == '') {
                return false;
            }

            return $Token[1];
        } catch (Exception $e) {
            return false;
        }
    }

    public function getAndDecodeToken()
    {
        $token = Self::validateToken();
        try {
            if ($token !== false) {
                // Query the database for the token and decode it....
                $TokenModel = new ModelsToken();
                // Check the database for the query before decoding it...
                $tokenDB = $TokenModel->fetchToken($token);

                if ($tokenDB['status']) {
                    // decode the token and pass the result on to the controller....
                    $decodedToken = (array) JWT::decode($token, Self::JWTSecret(), array('HS256'));
                    if (isset($decodedToken['user_id'])) {
                        Self::$user_id = $decodedToken['user_id'];
                        return $decodedToken['user_id'];
                    }

                    return false;
                }
                return false;
            }

            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getUser()
    {
        try {
            $UserModel = new ModelsUsers();
            $user = $UserModel->getUserById(Self::$user_id);
            if (!empty($user)) {
                return $user['data'];
            }

            return [];
        } catch (Exception $e) {
            return [];
        }
    }
}