<?php
header("Content-Type: application/json; charset=UTF-8");
require_once('vendor/autoload.php');

use MVC\Controller;

use \Firebase\JWT\JWT;

class ControllersUsers extends Controller
{

    public function all()
    {

        // Connect to database
        $model = $this->model('users');

        // Read All Task
        $users = $model->getAllUsers();

        // Send Response
        $this->response->sendStatus(200);
        $this->response->setContent(array("users" => $users->rows, "total" => $users->num_rows));
    }

    public function getAllGuests()
    {

        $model = $this->model('users');

        $users = $model->getAllGuests();

        $this->response->sendStatus(200);
        $this->response->setContent(array("guests" => $users->rows, "total" => $users->num_rows));
    }

    public function adminSignIn()
    {

        header("Content-Type: application/json; charset=UTF-8");
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }

        $model = $this->model('users');

        // Read All Task
        $users = $model->getSpecialUser($request['email'], $request['password']);
        // Send Response
        if ($users->num_rows < 1) {
            $this->response->sendStatus(404);
            $this->response->setContent(["status" => 404, "msg" => "No account associated to those credentials"]);
            return;
        }
        $middleware = $this->middleware('JwtMiddleware');
        $tokenSecret =  $middleware->JWTSecret();
        $tokenPayload = array(
            'iat' => time(),
            'iss' => 'umutibaapi',
            "exp" => strtotime('+ 7 Days'),
            "user_id" => $users->rows[0]['user_id'],
            "email" => $users->rows[0]['email'],
            "mobile_no" => $users->rows[0]['mobile_no'],
            "full_name" => $users->rows[0]['full_name'],
            "account_type" => $users->rows[0]['account_type']
        );
        $Jwt = JWT::encode($tokenPayload, $tokenSecret, 'RS256');
        $this->response->sendStatus(200);
        $this->response->setContent(["users" => $users->rows[0], "token" => $Jwt]);
    }

    public function signUp()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $model = $this->model('users');
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }

        if (empty($request['full_name']) || empty($request['email']) || empty($request['phone']) || empty($request['password'])) {
            $this->response->sendStatus(404);
            $this->response->setContent(["status" => 404, "msg" => "Some fields aren't set!!!"]);
        } else if ($model->checkVerification($request['phone'])->num_rows > 0) {
            $this->response->sendStatus(401);
            $this->response->setContent(["status" => 401, "msg" => "User is active but not verified"]);
        } else if ($model->getUser($request['phone'])->num_rows > 0) {
            $this->response->sendStatus(403);
            $this->response->setContent(["status" => 403, "msg" => "User already exists here"]);
        } else if ($model->getUserByEmail($request['email'])->num_rows > 0) {
            $this->response->sendStatus(403);
            $this->response->setContent(["status" => 403, "msg" => "Email already exists here"]);
        } else {
            // Save new Data
            $response = $model->saveUser($request);
            if ($response == false) {
                $this->response->sendStatus(500);
                $this->response->setContent(["status" => 500, "msg" => "Something went wrong!!!"]);
            } else {
                $this->response->sendStatus(201);
                $this->response->setContent(["status" => 201, "msg" => "Data saved successfully!"]);
            }
        }
    }

    public function verify()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $model = $this->model('users');
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }

        if (empty($request['code']) || empty($request['phone'])) {
            $this->response->sendStatus(404);
            $this->response->setContent(["status" => 404, "msg" => "Some fields aren't set!!!"]);
        } else {
            // Save new Data
            $response = $model->activateUser($request);
            if ($response == 2) {
                $this->response->sendStatus(403);
                $this->response->setContent(["status" => 403, "msg" => "invalid code"]);
            } else if ($response == 1) {
                $this->response->sendStatus(200);
                $this->response->setContent(["status" => 200, "msg" => "verified successfully!"]);
            } else {
                $this->response->sendStatus(500);
                $this->response->setContent(["status" => 500, "msg" => "Something went wrong!!!"]);
            }
        }
    }

    public function signIn()
    {

        $model = $this->model('users');
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }
        if (empty($request['username']) || empty($request['password'])) {
            $this->response->setContent(["status" => 403, "msg" => "Some fields aren't set!!!"]);
        } else if ($model->authUser($request)->num_rows < 1) {
            $this->response->setContent(["status" => 404, "msg" => "Incorrect username or password"]);
        } else if ($model->authUser($request)->row['is_verified'] != 1) {
            $this->response->setContent(["status" => 403, "msg" => "Account is not verified yet"]);
        } else {
            $userData = $model->authUser($request)->row;
            $middleware = $this->middleware('JwtMiddleware');
            $tokenSecret =  $middleware->JWTSecret();
            $tokenPayload = array(
                'iat' => time(),
                'iss' => 'umutibaapi',
                "exp" => strtotime('+ 7 Days'),
                "user_id" => $userData['user_id']
            );
            $Jwt = JWT::encode($tokenPayload, $tokenSecret, 'RS256');

            // Save JWT Token...
            $TokenModel = $this->model('token');
            $TokenModel->createToken([
                'user_id' => $userData['user_id'],
                'jwt_token' => $Jwt
            ]);

            $this->response->setContent(["status" => 200, "data" => $userData, "token" => $Jwt]);
        }
    }

    public function getById()
    {
        $model = $this->model('users');
        $request = $this->request->request;
        foreach (array_keys($request) as &$tempKey) {
            $id = explode('/', $tempKey)[1];
        }

        $response = $model->getUserById($id);

        if ($response->num_rows > 0) {
            $this->response->sendStatus(200);
            $this->response->setContent($response->row);
        } else {
            $this->response->sendStatus(404);
            $this->response->setContent(["status" => 404, "msg" => "User not found"]);
        }
    }
}