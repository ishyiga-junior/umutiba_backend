<?php
header("Content-Type: application/json; charset=UTF-8");
require_once 'vendor/autoload.php';

use MVC\Controller;

require 'vendor/autoload.php';

class ControllersProducts extends Controller
{

    public function all()
    {

        // Connect to database
        $model = $this->model('products');

        // Read All Task
        $products = $model->getAllProducts();

        // Send Response
        $this->response->sendStatus(200);
        $this->response->setContent(array("products" => $products->rows, "total" => $products->num_rows));
    }

    public function loadniki()
    {
        // Connect to database
        header("Content-Type: application/xml; charset=UTF-8");
        $model = $this->model('products');
        // Read All Task
        $products = $model->getAllNikiProducts();
        // Send Response
        $prod = preg_replace(
            '/&(?!#?[a-z0-9]+;)/',
            "and",
            $products
        );
        $prod1 = preg_replace(
            '/>>(?!#?[a-z0-9]+;)/',
            ">",
            $prod
        );
        $prod2 =
            preg_replace(
            '/>\s(?!#?[a-z0-9]+;)/',
            ">",
            $prod1
        );

        $prod3 =
            preg_replace(
            '/\+<[\s\w](?!#?[a-z0-9]+;)/',
            " + less ",
            $prod2
        );
        $xmldata = simplexml_load_string(utf8_encode($prod3));

        $array = $xmldata->LINE;
        $data = array();
        foreach ($array as $tempKey) {
            $niki_code = (string) $tempKey->niki_code;
            $name = (string) $tempKey->item_commercial_name;
            $keywords = (string) $tempKey->key_word;

            array_push($data, array("niki_code" => $niki_code, "item_commercial_name" => $name, "item_inn" => $keywords));
        }
        $filteredArray["LINE"] = $data;
        $this->response->sendStatus(200);
        $this->response->setContent($filteredArray);
    }

    public function getProductsById()
    {
        $model = $this->model('products');
        $request = $this->request->request;
        foreach (array_keys($request) as &$tempKey) {
            $id = explode('/', $tempKey)[1];
        }

        $response = $model->getSpecificUserProducts($id);

        if ($response->num_rows > 0) {
            $this->response->sendStatus(200);
            $this->response->setContent($response->rows);
        } else {
            $this->response->sendStatus(404);
            $this->response->setContent(["status" => 404, "msg" => "User not found"]);
        }
    }

    public function getProductsByCity()
    {
        $model = $this->model('products');
        $request = $this->request->request;
        foreach (array_keys($request) as &$tempKey) {
            $city = explode('/', $tempKey)[1];
        }

        $response = $model->getAllProductsBySpecificCity($city);

        if ($response->num_rows > 0) {
            $this->response->sendStatus(200);
            $this->response->setContent(["data" => $response->rows, "total" => $response->num_rows]);
        } else {
            $this->response->sendStatus(404);
            $this->response->setContent(["status" => 404, "msg" => "No data for this city"]);
        }
    }

    public function addProduct()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $model = $this->model('products');
        $userModel = $this->model('users');
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }
        if (!empty($request['user_id']) && ($userModel->getUserById($request['user_id'])->num_rows < 1)) {
            $this->response->sendStatus(403);
            $this->response->setContent(["status" => 403, "msg" => "This account not found"]);
        } else {
            // Save new Data
            $response = $model->saveProduct($request, $this->request->files);
            if ($response == 404) {
                $this->response->sendStatus($response);
                $this->response->setContent(["status" => $response, "msg" => "following fields are required: niki_code, name,
        price, quantity, location, image, and user_id or device_id"]);
            } else {
                $this->response->sendStatus(201);
                $this->response->setContent(["status" => 201, "msg" => "Data saved successfully!"]);
            }
        }
    }

    public function postLangues()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }
        $file = fopen(__DIR__ . '/langues.json', 'w');
        fwrite($file, json_encode($request));
        fclose($file);

        $this->response->sendStatus(201);
        $this->response->setContent(["status" => 201, "msg" => "Data saved!"]);
    }

    public function getLangues()
    {
        $readJSONFile = file_get_contents(__DIR__ . '/corrected_langues.json');
        $array = json_decode($readJSONFile);
        $this->response->sendStatus(200);
        $this->response->setContent($array);
    }

    public function getEveryOnesLangues()
    {
        $request = $this->request->request;
        foreach (array_keys($request) as &$tempKey) {
            $total = explode('/', $tempKey)[1];
        }
        $readJSONFile = file_get_contents(__DIR__ . '/langues.json');
        $array = json_decode($readJSONFile);
        foreach ($array as $key => $val) {
            $arr = array_slice($array->$key, ($total - 1) * 42, 42);
        }
        $this->response->sendStatus(200);
        $this->response->setContent($arr);
    }

    public function postEveryOnesLangues()
    {
        header("Content-Type: application/json; charset=UTF-8");
        $request = $this->request->request;
        if (count($request) < 2) {
            $request = json_decode(file_get_contents('php://input'), true);
        }
        $inp = json_decode(file_get_contents(__DIR__ . '/corrected_langues.json'));
        $tempArray = $inp;
        array_push($tempArray, ...$request);
        $jsonData = json_encode($tempArray);
        file_put_contents(__DIR__ . '/corrected_langues.json', $jsonData);
        $this->response->sendStatus(200);
        $this->response->setContent($tempArray);
    }
}