<?php

$router->get('/users/all', 'users@all');

$router->get('/users/guests', 'users@getAllGuests');

$router->get('/users/:id', 'users@getById');

$router->get('/home index', 'home@index');

$router->post('/upload', 'home@uploadImage');

$router->post('/home', 'home@post');

$router->get('/', function ($param) {
    echo 'Welcome to Umutiba API service';
});

$router->post('/signup', 'users@signUp');

$router->post('/login', 'users@signIn');

$router->post('/verify', 'users@verify');

$router->post('/admin-login', 'users@adminSignIn');

$router->post('/product/add', 'products@addProduct');

$router->get('/products/all', 'products@all');

$router->get('/products/niki', 'products@loadniki');

$router->get("/user/:id/products", 'products@getProductsById');

$router->get("/products/:city", 'products@getProductsByCity');